$.ajax({
    method: "POST",
    url: "/stats/info"
}).done(function (response) {
    console.log(response);
    response.forEach(function (item, index) {
        var names = {}
            , values = [];

        item.forEach(function (coin, index) {
            names[index] = coin.handle;
            values.push(coin.coinUsdValue);
        });

        $('#sparkline' + index).sparkline(
            values,
            {
                type: 'pie',
                height: '300',
                resize: true,
                sliceColors: [ '#009efb', '#55ce63', '#f1f2f7', '#ff6666', '#ffff99', '#66ff99', '#cc66ff', '#cccc00' ],
                tooltipFormat: '{{offset:offset}} - {{value}} USD',
                tooltipValueLookups: {
                    offset: names
                },
            }
        );
    });
});