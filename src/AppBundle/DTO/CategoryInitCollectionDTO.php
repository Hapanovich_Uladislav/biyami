<?php

namespace AppBundle\DTO;

use AppBundle\Entity\Category;

class CategoryInitCollectionDTO implements \JsonSerializable
{
    private $categories;

    /**
     * @param Category[] $categories
     */
    public function __construct(array $categories)
    {
        $this->categories = $categories;
    }

    public function jsonSerialize()
    {
        $response = [];

        for ($i = 0; $i < count($this->categories); $i++) {
            $response[$this->categories[$i]->getTypeName()][] = $this->categories[$i];
        }

        return $response;
    }
}
