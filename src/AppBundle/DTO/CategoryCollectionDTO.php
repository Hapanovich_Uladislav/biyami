<?php

namespace AppBundle\DTO;

use AppBundle\Entity\Category;

class CategoryCollectionDTO implements \JsonSerializable
{
    private $categories;

    /**
     * @param Category[] $categories
     */
    public function __construct(array $categories)
    {
        $this->categories = $categories;
    }

    public function jsonSerialize()
    {
        $response = [];

        for ($i = 0; $i < count($this->categories); $i++) {
            $response[$this->categories[$i]->getType()][] = $this->categories[$i]->jsonSerializeNotHide();
        }

        return $response;
    }
}