<?php

namespace AppBundle\Entity;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use AppBundle\Traits\UploadFileTrait;

/**
 *
 * @ORM\Table(name="tblIndustry")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\IndustryRepository")
 *
 */
class Industry implements \JsonSerializable
{
    use UploadFileTrait;
    const UPLOADED_ROOT_DIR = 'uploads';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $title = '';

    /**
     * @var string
     *
     * @ORM\Column(name="imageUrl", type="string", length=255, nullable=true)
     */
    private $imageUrl;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $isRemove = false;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Category", mappedBy="industry")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $category;

    /**
     * @var string
     *
     * @ORM\Column(name="bigImageUrl", type="string", length=255, nullable=true)
     */
    private $bigImageUrl;

    /**
     * @var File
     *
     * @Assert\File(mimeTypes={"image/png", "image/jpeg"})
     */
    public $bigFile;

    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->setCreatedAt();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * @return bool
     */
    public function isRemove(): bool
    {
        return $this->isRemove;
    }

    public function remove()
    {
        $this->isRemove = true;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @return Category[]
     */
    public function getCategories(): array
    {
        return $this->category->filter(
            function (Category $category) {
                return !$category->isRemove();
            }
        )->getValues();
    }


    /**
     * @return string
     */
    public function getImageUrl(): ?string
    {
        return $this->imageUrl;
    }

    /**
     * @param string $imageUrl
     */
    public function setImageUrl(string $imageUrl)
    {
        $this->imageUrl = $imageUrl;
    }

    /**
     * @return null|string
     */
    public function getImage(): ?string
    {
        return self::UPLOADED_ROOT_DIR . '/' . $this->imageUrl;
    }

    /**
     * @return null|string
     */
    public function getFilePath(): ?string
    {
        return $this->imageUrl;
    }

    /**
     * @return null|string
     */
    public function getUploadRootDir(): ?string
    {
        return self::UPLOADED_ROOT_DIR;
    }

    /**
     * @param string $path
     */
    public function setFilePath(string $path): void
    {
        $this->imageUrl = $path;
    }




    /**
     * @return string
     */
    public function getBigImageUrl(): ?string
    {
        return $this->bigImageUrl;
    }

    /**
     * @param string $bigImageUrl
     */
    public function setBigImageUrl(string $bigImageUrl)
    {
        $this->bigImageUrl = $bigImageUrl;
    }

    /**
     * @return null|string
     */
    public function getBigImage(): ?string
    {
        return self::UPLOADED_ROOT_DIR . '/' . $this->bigImageUrl;
    }

    /**
     * @return null|string
     */
    public function getBigFilePath(): ?string
    {
        return $this->bigImageUrl;
    }

    /**
     * @param string $path
     */
    public function setBigFilePath(string $path): void
    {
        $this->bigImageUrl = $path;
    }


    /**
     * @return void
     */
    public function resetBigFile(): void
    {
        $this->bigFile = null;
    }

    /**
     * @return File|null
     */
    public function getBigFile(): ?File
    {
        return $this->bigFile;
    }

    /**
     * @return void
     */
    public function uploadBig(): void
    {
        /** @var UploadedFile $file */
        $file = $this->getBigFile();

        if (null === $file) {
            return;
        }

        $extension = substr($file->getClientOriginalName(), strripos($file->getClientOriginalName(), '.'));
        $name = md5_file($file) . $extension;

        $file->move($this->getUploadRootDir(), $name);

        $this->setBigFilePath($name);
        $this->resetBigFile();
    }


    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'image' => $this->getImage(),
            'bigImage' => $this->getBigImage(),
            'createdAt' => $this->createdAt->getTimestamp(),
            'categories' => $this->getCategories()
        ];
    }
}
