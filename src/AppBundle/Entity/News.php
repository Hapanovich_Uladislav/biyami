<?php

namespace AppBundle\Entity;

use AppBundle\Traits\UploadFileTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 *
 * @ORM\Table(name="tblNews")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\NewsRepository")
 *
 */
class News implements \JsonSerializable
{
    use UploadFileTrait;
    const UPLOADED_ROOT_DIR = 'uploads';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $title = '';

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     */
    private $shortDescription = '';

    /**
     * @var string
     *
     * @ORM\Column(name="imageUrl", type="string", length=255, nullable=true)
     */
    private $imageUrl;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     */
    private $text = '';

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $isRemove = false;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $isHide = false;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    public function __construct()
    {
        $this->setCreatedAt();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getShortDescription(): string
    {
        return $this->shortDescription;
    }

    /**
     * @param string $shortDescription
     */
    public function setShortDescription(string $shortDescription)
    {
        $this->shortDescription = $shortDescription;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text)
    {
        $this->text = $text;
    }

    /**
     * @return bool
     */
    public function isRemove(): bool
    {
        return $this->isRemove;
    }

    public function remove()
    {
        $this->isRemove = true;
    }

    /**
     * @return bool
     */
    public function isHide(): bool
    {
        return $this->isHide;
    }

    public function hide()
    {
        $this->isHide = true;
    }

    public function show()
    {
        $this->isHide = false;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();
    }


    /**
     * @return string
     */
    public function getImageUrl(): ?string
    {
        return $this->imageUrl;
    }

    /**
     * @param string $imageUrl
     */
    public function setImageUrl(string $imageUrl)
    {
        $this->imageUrl = $imageUrl;
    }

    /**
     * @return null|string
     */
    public function getImage(): ?string
    {
        return self::UPLOADED_ROOT_DIR . '/' . $this->imageUrl;
    }

    /**
     * @return null|string
     */
    public function getFilePath(): ?string
    {
        return $this->imageUrl;
    }

    /**
     * @return null|string
     */
    public function getUploadRootDir(): ?string
    {
        return self::UPLOADED_ROOT_DIR;
    }

    /**
     * @param string $path
     */
    public function setFilePath(string $path): void
    {
        $this->imageUrl = $path;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'shortDescription' => $this->shortDescription,
            'text' => $this->text,
            'image' => $this->getImage(),
            'createdAt' => $this->createdAt->getTimestamp(),
        ];
    }
}
