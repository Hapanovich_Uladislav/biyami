<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Traits\UploadFileTrait;

/**
 *
 * @ORM\Table(name="tblPartner")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PartnerRepository")
 *
 */
class Partner implements \JsonSerializable
{
    use UploadFileTrait;
    const UPLOADED_ROOT_DIR = 'uploads';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $link = '';

    /**
     * @var string
     *
     * @ORM\Column(name="imageUrl", type="string", length=255, nullable=true)
     */
    private $imageUrl;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $isRemove = false;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $isHide = false;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    public function __construct()
    {
        $this->setCreatedAt();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->link;
    }

    /**
     * @param string $link
     */
    public function setLink(string $link)
    {
        $this->link = $link;
    }

    /**
     * @return bool
     */
    public function isRemove(): bool
    {
        return $this->isRemove;
    }

    public function remove()
    {
        $this->isRemove = true;
    }

    /**
     * @return bool
     */
    public function isHide(): bool
    {
        return $this->isHide;
    }

    public function hide()
    {
        $this->isHide = true;
    }

    public function show()
    {
        $this->isHide = false;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();
    }


    /**
     * @return string
     */
    public function getImageUrl(): ?string
    {
        return $this->imageUrl;
    }

    /**
     * @param string $imageUrl
     */
    public function setImageUrl(string $imageUrl)
    {
        $this->imageUrl = $imageUrl;
    }

    /**
     * @return null|string
     */
    public function getImage(): ?string
    {
        return self::UPLOADED_ROOT_DIR . '/' . $this->imageUrl;
    }

    /**
     * @return null|string
     */
    public function getFilePath(): ?string
    {
        return $this->imageUrl;
    }

    /**
     * @return null|string
     */
    public function getUploadRootDir(): ?string
    {
        return self::UPLOADED_ROOT_DIR;
    }

    /**
     * @param string $path
     */
    public function setFilePath(string $path): void
    {
        $this->imageUrl = $path;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'link' => $this->link,
            'image' => $this->getImage(),
            'createdAt' => $this->createdAt->getTimestamp(),
        ];
    }
}
