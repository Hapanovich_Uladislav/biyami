<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *
 * @ORM\Table(name="tblOrder")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OrderRepository")
 *
 */
class Order implements \JsonSerializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $name = '';

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $phone = '';

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     */
    private $orderText = '';

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $isRemove = false;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $isProcessed = false;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    public function __construct()
    {
        $this->setCreatedAt();
    }

    public function getId(): int
    {
        return $this->id;
    }


    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }


    public function getPhone(): string
    {
        return $this->phone;
    }

    public function setPhone(string $phone)
    {
        $this->phone = $phone;
    }


    public function getOrderText(): string
    {
        return $this->orderText;
    }

    public function setOrderText(string $orderText)
    {
        $this->orderText = $orderText;
    }


    public function isRemove(): bool
    {
        return $this->isRemove;
    }

    public function remove()
    {
        $this->isRemove = true;
    }


    public function isProcessed(): bool
    {
        return $this->isProcessed;
    }

    public function done()
    {
        $this->isProcessed = true;
    }

    public function undone()
    {
        $this->isProcessed = false;
    }


    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();
    }


    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'phone' => $this->phone,
            'orderText' => $this->orderText,
            'isProcessed' => $this->isProcessed,
            'createdAt' => $this->createdAt,
        ];
    }
}
