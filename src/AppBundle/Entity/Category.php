<?php

namespace AppBundle\Entity;

use AppBundle\Traits\UploadFileTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 *
 * @ORM\Table(name="tblCategory")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CategoryRepository")
 *
 */
class Category implements \JsonSerializable
{
    use UploadFileTrait;
    const UPLOADED_ROOT_DIR = 'uploads';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $title = '';

    /**
     * @var string
     *
     * @ORM\Column(name="imageUrl", type="string", length=255, nullable=true)
     */
    private $imageUrl;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $isRemove = false;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Product", mappedBy="category")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $products;

    /**
     * @var Industry
     *
     * @ORM\ManyToOne(targetEntity="Industry", inversedBy="categories")
     * @ORM\JoinColumn(name="industry", referencedColumnName="id")
     */
    private $industry;

    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->setCreatedAt();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    public function getTypeName(): string
    {
        return $this->industry->getTitle();
    }

    /**
     * @return bool
     */
    public function isRemove(): bool
    {
        return $this->isRemove;
    }

    public function remove()
    {
        $this->isRemove = true;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @return Product[]
     */
    public function getProducts(): array
    {
        return $this->products->filter(
            function (Product $product) {
                return !$product->isRemove();
            }
        )->getValues();
    }

    /**
     * @return Product[]
     */
    public function getVisibleProducts(): array
    {
        return $this->products->filter(
            function (Product $product) {
                return !$product->isRemove() || !$product->isHide();
            }
        )->getValues();
    }

    /**
     * @return null|Industry
     */
    public function getIndustry(): ?Industry
    {
        return $this->industry;
    }

    /**
     * @param Industry $industry
     */
    public function setIndustry(Industry $industry)
    {
        $this->industry = $industry;
    }



    /**
     * @return string
     */
    public function getImageUrl(): ?string
    {
        return $this->imageUrl;
    }

    /**
     * @param string $imageUrl
     */
    public function setImageUrl(string $imageUrl)
    {
        $this->imageUrl = $imageUrl;
    }

    /**
     * @return null|string
     */
    public function getImage(): ?string
    {
        return self::UPLOADED_ROOT_DIR . '/' . $this->imageUrl;
    }

    /**
     * @return null|string
     */
    public function getFilePath(): ?string
    {
        return $this->imageUrl;
    }

    /**
     * @return null|string
     */
    public function getUploadRootDir(): ?string
    {
        return self::UPLOADED_ROOT_DIR;
    }

    /**
     * @param string $path
     */
    public function setFilePath(string $path): void
    {
        $this->imageUrl = $path;
    }

    public function removeImage()
    {
        $this->imageUrl = null;
    }


    public function jsonSerializeNotHide()
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'createdAt' => $this->createdAt->getTimestamp(),
            'product' => $this->getVisibleProducts()
        ];
    }


    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'image' => $this->getImage(),
            'createdAt' => $this->createdAt->getTimestamp(),
            'product' => $this->getProducts()
        ];
    }
}
