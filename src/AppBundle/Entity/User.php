<?php

namespace AppBundle\Entity;

use AppBundle\Helper\HashHelper;
use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="tblUser")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="hash", type="string", length=30, nullable=false, unique=true)
     */
    private $hash;

    /**
     * @var self
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="childrenUsers")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    private $parentUser;

    /**
     * @var self
     *
     * @ORM\OneToMany(targetEntity="User", mappedBy="parentUser")
     */
    private $childrenUsers;

    public function __construct()
    {
        parent::__construct();

        $this->generateUsername();
        $this->generateHash();
        $this->childrenUsers = new ArrayCollection;
    }

    /**
     * Generate user username
     */
    private function generateUsername()
    {
        $this->username = HashHelper::getSimpleHashByLength(8);
    }

    /**
     * Generate user hash
     */
    private function generateHash()
    {
        $this->hash = HashHelper::getSimpleHashByLength(30);
    }

    /**
     * @return string
     */
    public function getHash(): string
    {
        return $this->hash;
    }

    /**
     * Activate user
     */
    public function activate()
    {
        $this->enabled = true;
    }

    /**
     * @param User $parentUser
     */
    public function setParentUser(User $parentUser): void
    {
        $this->parentUser = $parentUser;
    }
}
