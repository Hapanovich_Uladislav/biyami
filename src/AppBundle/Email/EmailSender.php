<?php

namespace AppBundle\Email;

use AppBundle\Entity\User;
use \Swift_Mailer;
use \Twig_Environment;
use \Swift_Message;

/**
 * Class EmailSender.
 */
class EmailSender
{
    CONST SITE_URL = 'localhost:8000';
    CONST FROM_NAME = 'GD Bounty';
    CONST REGISTRATION_SUBJECT = 'Подтверждение регистрации в GD Bounty';
    CONST RECOVERY_SUBJECT = 'Восстановление пароля в GD Bounty';
    /**
     * @var Swift_Mailer
     */
    private $mailer;

    /**
     * @var Twig_Environment
     */
    private $twig;

    /**
     * @var string
     */
    private $fromEmail;

    /**
     * @param Swift_Mailer     $mailer
     * @param Twig_Environment $twig
     * @param string           $fromEmail
     */
    public function __construct(Swift_Mailer $mailer, Twig_Environment $twig, string $fromEmail)
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->fromEmail = $fromEmail;
    }

    /**
     * @param User   $user
     * @param string $confirmationLink
     *
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function sendRegistrationConfirmation(User $user, string $confirmationLink)
    {
        $logger = new \Swift_Plugins_Loggers_ArrayLogger();
        $this->mailer->registerPlugin(new \Swift_Plugins_LoggerPlugin($logger));

        $message = Swift_Message::newInstance()
            ->setSubject(self::REGISTRATION_SUBJECT)
            ->setFrom($this->fromEmail, self::FROM_NAME)
            ->setTo($user->getEmail())
            ->setBody(
                $this->twig->render('@app/Email/registration_confirmation.html.twig', [
                    'subject' => self::REGISTRATION_SUBJECT,
                    'siteUrl' => self::SITE_URL,
                    'user' => $user,
                    'confirmationLink' => $confirmationLink,
                ]),
                'text/html'
            );

        $this->mailer->send($message, $errors);
    }

    /**
     * @param User   $user
     * @param string $recoveryLink
     *
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function sendPasswordRecoveryMessage(User $user, string $recoveryLink)
    {
        $logger = new \Swift_Plugins_Loggers_ArrayLogger();
        $this->mailer->registerPlugin(new \Swift_Plugins_LoggerPlugin($logger));

        $message = Swift_Message::newInstance()
            ->setSubject(self::RECOVERY_SUBJECT)
            ->setFrom($this->fromEmail, self::FROM_NAME)
            ->setTo($user->getEmail())
            ->setBody(
                $this->twig->render('@app/Email/password_recovery.html.twig', [
                    'subject' => self::REGISTRATION_SUBJECT,
                    'siteUrl' => self::SITE_URL,
                    'user' => $user,
                    'recoveryLink' => $recoveryLink,
                ]),
                'text/html'
            );

        $this->mailer->send($message, $errors);
    }
}