<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use AppBundle\Entity\News;
use AppBundle\Entity\Partner;
use AppBundle\Entity\Industry;
use AppBundle\DTO\CategoryInitCollectionDTO;
use AppBundle\Repository\NewsRepository;
use AppBundle\Repository\PartnerRepository;
use AppBundle\Repository\IndustryRepository;

class MainController extends AbstractController
{
    public function initAction()
    {
        $em = $this->getDoctrine()->getManager();

        /** @var NewsRepository $newsRepository */
        $newsRepository = $em->getRepository(News::class);
        /** @var PartnerRepository $partnerRepository */
        $partnerRepository = $em->getRepository(Partner::class);
        /** @var IndustryRepository $industryRepository */
        $industryRepository = $em->getRepository(Industry::class);

        $lastNews = $newsRepository->lastNews(3);
        $partners = $partnerRepository->firstPartners(5);
        $industries = $industryRepository->findAllIndustries();

        return new JsonResponse([
            'news' => $lastNews,
            'partners' => $partners,
            'industries' => $industries
        ]);
    }

    public function errorAction()
    {
        return $this->render('@app/admin_main.html.twig');
    }

}
