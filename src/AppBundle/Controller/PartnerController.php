<?php

namespace AppBundle\Controller;

use AppBundle\Form\EditPartnerType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Partner;
use AppBundle\Form\PartnerType;
use AppBundle\Repository\PartnerRepository;

class PartnerController extends Controller
{
    public function partnerListAction()
    {
        $partners = $this->getDoctrine()->getRepository(Partner::class)->findBy([
            'isRemove' => false
        ]);

        return new JsonResponse($partners);
    }

    public function allPartnerListAction(Request $request)
    {
        /** @var PartnerRepository $partnerRepository */
        $partnerRepository = $this->getDoctrine()->getRepository(Partner::class);
        $partners = $partnerRepository->getPartnersAllQuery();

        $pagination = $this->get('knp_paginator')->paginate(
            $partners,
            $request->query->getInt('page', 1),
            20
        );

        return $this->render('@app/Partner/index.html.twig', [
            'partners' => $pagination
        ]);

    }

    public function createPartnerAction(Request $request)
    {
        $errors = [];
        $em = $this->getDoctrine()->getManager();

        $partner = new Partner();

        $form = $this->createForm(PartnerType::class, $partner);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $validator = $this->get('validator');
            $errors = $validator->validate($partner);

            if (!$errors->count()) {
                $partner->upload();
                $em->persist($partner);
                $em->flush();

                return $this->redirectToRoute('partner_all');
            }

            foreach ($form->getErrors() as $error) {
                $errors[] = $error->getMessage();
            }
        }

        return $this->render('@app/Partner/create_partner.html.twig', [
            'form' => $form->createView(),
            'errors' => $errors
        ]);
    }

    public function editPartnerAction(Request $request, Partner $partner)
    {
        $errors = [];
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(EditPartnerType::class, $partner);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $validator = $this->get('validator');
            $errors = $validator->validate($partner);

            if (!$errors->count()) {
                $partner->upload();
                $em->flush();

                return $this->redirectToRoute('partner_all');
            }

            foreach ($form->getErrors() as $error) {
                $errors[] = $error->getMessage();
            }
        }

        return $this->render('@app/Partner/edit_partner.html.twig', [
            'form' => $form->createView(),
            'partner' => $partner,
            'errors' => $errors
        ]);
    }

    public function removePartnerAction(Partner $partner)
    {
        $partner->remove();
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('partner_all');
    }

    public function hidePartnerAction(Partner $partner)
    {
        $partner->hide();
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('partner_all');
    }

    public function showPartnerAction(Partner $partner)
    {
        $partner->show();
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('partner_all');
    }
}
