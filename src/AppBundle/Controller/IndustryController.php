<?php

namespace AppBundle\Controller;

use AppBundle\Form\EditIndustryType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Industry;
use AppBundle\Form\IndustryType;
use AppBundle\Repository\IndustryRepository;

class IndustryController extends Controller
{
    public function industryListAction()
    {
        /** @var IndustryRepository $industryRepository */
        $industryRepository = $this->getDoctrine()->getRepository(Industry::class);
        $industries = $industryRepository->findAllIndustries();

        return new JsonResponse($industries);
    }

    public function allIndustryListAction(Request $request)
    {
        /** @var IndustryRepository $industryRepository */
        $industryRepository = $this->getDoctrine()->getRepository(Industry::class);
        $industries = $industryRepository->getAllIndustriesQuery();

        $pagination = $this->get('knp_paginator')->paginate(
            $industries,
            $request->query->getInt('page', 1),
            20
        );

        return $this->render('@app/Industry/index.html.twig', [
            'industries' => $pagination
        ]);
    }

    public function createIndustryAction(Request $request)
    {
        $errors = [];
        $em = $this->getDoctrine()->getManager();

        $industry = new Industry();

        $form = $this->createForm(IndustryType::class, $industry);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $validator = $this->get('validator');
            $errors = $validator->validate($industry);

            if (!$errors->count()) {
                $industry->upload();
                $industry->uploadBig();
                $em->persist($industry);
                $em->flush();

                return $this->redirectToRoute('industry_all');
            }

            foreach ($form->getErrors() as $error) {
                $errors[] = $error->getMessage();
            }
        }

        return $this->render('@app/Industry/create_industry.html.twig', [
            'form' => $form->createView(),
            'industry' => $industry,
            'errors' => $errors
        ]);
    }

    public function editIndustryAction(Request $request, Industry $industry)
    {
        $errors = [];
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(EditIndustryType::class, $industry);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $validator = $this->get('validator');
            $errors = $validator->validate($industry);

            if (!$errors->count()) {
                $industry->upload();
                $industry->uploadBig();
                $em->flush();

                return $this->redirectToRoute('industry_all');
            }

            foreach ($form->getErrors() as $error) {
                $errors[] = $error->getMessage();
            }
        }

        return $this->render('@app/Industry/edit_industry.html.twig', [
            'form' => $form->createView(),
            'industry' => $industry,
            'errors' => $errors
        ]);
    }

    public function removeIndustryAction(Industry $industry)
    {
        $industry->remove();
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('industry_all');
    }
}