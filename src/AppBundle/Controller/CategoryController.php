<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Category;
use AppBundle\Form\CategoryType;
use AppBundle\DTO\CategoryCollectionDTO;
use AppBundle\Repository\CategoryRepository;

class CategoryController extends Controller
{
    public function categoryListAction()
    {
        /** @var CategoryRepository $categoryRepository */
        $categoryRepository = $this->getDoctrine()->getRepository(Category::class);
        $categories = $categoryRepository->findAllCategories();

        return new JsonResponse(new CategoryCollectionDTO($categories));
    }

    public function allCategoryListAction(Request $request)
    {
        /** @var CategoryRepository $categoryRepository */
        $categoryRepository = $this->getDoctrine()->getRepository(Category::class);
        $categories = $categoryRepository->getAllCategoriesQuery();

        $pagination = $this->get('knp_paginator')->paginate(
            $categories,
            $request->query->getInt('page', 1),
            20
        );

        return $this->render('@app/Category/index.html.twig', [
            'categories' => $pagination
        ]);
    }

    public function createCategoryAction(Request $request)
    {
        $errors = [];
        $em = $this->getDoctrine()->getManager();

        $category = new Category();

        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $validator = $this->get('validator');
            $errors = $validator->validate($category);

            if (!$errors->count()) {
                $category->upload();
                $em->persist($category);
                $em->flush();

                return $this->redirectToRoute('category_all');
            }

            foreach ($form->getErrors() as $error) {
                $errors[] = $error->getMessage();
            }
        }

        return $this->render('@app/Category/create_category.html.twig', [
            'form' => $form->createView(),
            'category' => $category,
            'errors' => $errors
        ]);
    }

    public function editCategoryAction(Request $request, Category $category)
    {
        $errors = [];
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $validator = $this->get('validator');
            $errors = $validator->validate($category);

            if (!$errors->count()) {
                $category->upload();
                $em->flush();

                return $this->redirectToRoute('category_all');
            }

            foreach ($form->getErrors() as $error) {
                $errors[] = $error->getMessage();
            }
        }

        return $this->render('@app/Category/edit_category.html.twig', [
            'form' => $form->createView(),
            'category' => $category,
            'errors' => $errors
        ]);
    }

    public function removeCategoryAction(Category $category)
    {
        $category->remove();
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('category_all');
    }

    public function removeImageAction(Category $category)
    {
        $category->removeImage();
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('category_all');
    }
}