<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use AppBundle\Entity\Order;

class OrderController extends AbstractController
{
    public function ordersListAction()
    {
        $orders = $this->getDoctrine()->getRepository(Order::class)->findBy([
            'isRemove' => false
        ]);

        return new JsonResponse($orders);
    }

    public function createOrderAction(Request $request)
    {
        $orderData = \json_decode($request->getContent(), true);
        $em = $this->getDoctrine()->getManager();

        $order = new Order();
        $order->setName($orderData['name']);
        $order->setPhone($orderData['phone']);
        $order->setOrderText($orderData['order']);

        $em->persist($order);
        $em->flush();

        return new JsonResponse($order);
    }

    public function removeOrderAction(Order $order)
    {
        $order->remove();
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('admin_order_list');
    }

    public function doneOrderAction(Order $order)
    {
        $order->done();
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('admin_order_list');
    }

    public function undoneOrderAction(Order $order)
    {
        $order->undone();
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('admin_order_list');
    }
}