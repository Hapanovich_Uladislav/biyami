<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Order;
use AppBundle\Entity\Category;

class AdminController extends Controller
{
    public function orderListAction(Request $request)
    {
        $orders = $this->getDoctrine()->getRepository(Order::class)->getOrderListQuery();

        $pagination = $this->get('knp_paginator')->paginate(
            $orders,
            $request->query->getInt('page', 1),
            20
        );

        return $this->render('@app/Admin/index.html.twig', [
            'orders' => $pagination
        ]);
    }

    public function categoriesListAction()
    {
        $categories = $this->getDoctrine()->getRepository(Category::class)->findAll();

        return $this->render('@app/Admin/index.html.twig', [
            'categories' => $categories
        ]);
    }
}
