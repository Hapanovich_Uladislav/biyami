<?php

namespace AppBundle\Controller;

use AppBundle\Form\ProductType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Product;
use AppBundle\Repository\ProductRepository;

class ProductController extends Controller
{
    public function allProductListAction(Request $request)
    {
        /** @var ProductRepository $productRepository */
        $productRepository = $this->getDoctrine()->getRepository(Product::class);
        $products = $productRepository->getAllProductsQuery();

        $pagination = $this->get('knp_paginator')->paginate(
            $products,
            $request->query->getInt('page', 1),
            20
        );

        return $this->render('@app/Product/index.html.twig', [
            'products' => $pagination
        ]);
    }

    public function createProductAction(Request $request)
    {
        $errors = [];
        $em = $this->getDoctrine()->getManager();

        $product = new Product();

        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $validator = $this->get('validator');
            $errors = $validator->validate($product);

            if (!$errors->count()) {
                $em->persist($product);
                $em->flush();

                return $this->redirectToRoute('product_all');
            }

            foreach ($form->getErrors() as $error) {
                $errors[] = $error->getMessage();
            }
        }

        return $this->render('@app/Product/create_product.html.twig', [
            'form' => $form->createView(),
            'product' => $product,
            'errors' => $errors
        ]);
    }

    public function editProductAction(Request $request, Product $product)
    {
        $errors = [];
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $validator = $this->get('validator');
            $errors = $validator->validate($product);

            if (!$errors->count()) {
                $em->flush();

                return $this->redirectToRoute('product_all');
            }

            foreach ($form->getErrors() as $error) {
                $errors[] = $error->getMessage();
            }
        }

        return $this->render('@app/Product/edit_product.html.twig', [
            'form' => $form->createView(),
            'product' => $product,
            'errors' => $errors
        ]);
    }

    public function removeProductAction(Product $product)
    {
        $product->remove();
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('product_all');
    }

    public function hideProductAction(Product $product)
    {
        $product->hide();
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('product_all');
    }

    public function showProductAction(Product $product)
    {
        $product->show();
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('product_all');
    }
}