<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\News;
use AppBundle\Form\NewsType;
use AppBundle\Form\EditNewsType;
use AppBundle\Repository\NewsRepository;

class NewsController extends Controller
{
    public function allNewsListAction(Request $request)
    {
        /** @var NewsRepository $newsRepository */
        $newsRepository = $this->getDoctrine()->getRepository(News::class);
        $news = $newsRepository->getAllNewsQuery();

        $pagination = $this->get('knp_paginator')->paginate(
            $news,
            $request->query->getInt('page', 1),
            20
        );

        // Todo: add date field to all news
        return $this->render('@app/News/index.html.twig', [
            'news_list' => $pagination
        ]);
    }

    public function singleNewsPageAction(News $news)
    {
        return $this->render('@app/News/single.html.twig', [
            'news' => $news
        ]);
    }

    public function newsListAction()
    {
        $news = $this->getDoctrine()->getRepository(News::class)->findBy([
            'isRemove' => false,
            'isHide' => false
        ]);

        return new JsonResponse($news);
    }

    public function getNewsAction(News $news)
    {
        return new JsonResponse($news);
    }

    public function createNewsAction(Request $request)
    {
        $errors = [];
        $em = $this->getDoctrine()->getManager();

        $news = new News();

        $form = $this->createForm(NewsType::class, $news);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $validator = $this->get('validator');
            $errors = $validator->validate($news);

            if (!$errors->count()) {
                $news->upload();
                $em->persist($news);
                $em->flush();

                return $this->redirectToRoute('news_all');
            }

            foreach ($form->getErrors() as $error) {
                $errors[] = $error->getMessage();
            }
        }

        return $this->render('@app/News/create_news.html.twig', [
            'form' => $form->createView(),
            'errors' => $errors
        ]);
    }

    public function editNewsAction(Request $request, News $news)
    {
        $errors = [];
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(EditNewsType::class, $news);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $validator = $this->get('validator');
            $errors = $validator->validate($news);

            if (!$errors->count()) {
                $news->upload();
                $em->flush();

                return $this->redirectToRoute('news_all');
            }

            foreach ($form->getErrors() as $error) {
                $errors[] = $error->getMessage();
            }
        }

        return $this->render('@app/News/edit_news.html.twig', [
            'form' => $form->createView(),
            'news' => $news,
            'errors' => $errors
        ]);
    }

    public function removeNewsAction(News $news)
    {
        $news->remove();
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('news_all');
    }

    public function hideNewsAction(News $news)
    {
        $news->hide();
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('news_all');
    }

    public function showNewsAction(News $news)
    {
        $news->show();
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('news_all');
    }
}
