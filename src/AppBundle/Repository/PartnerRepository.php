<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class PartnerRepository extends EntityRepository
{
    public function firstPartners(int $count = 5)
    {
        $qb = $this->createQueryBuilder('p');
        $qe = $qb->expr();

        return $qb
            ->addSelect('p')
            ->andWhere($qe->neq('p.isRemove', ':status'))
            ->andWhere($qe->neq('p.isHide', ':status'))
            ->setParameters([
                'status' => true
            ])
            ->setMaxResults($count)
            ->getQuery()
            ->getResult();
    }

    public function getPartnersAllQuery()
    {
        $qb = $this->createQueryBuilder('p');
        $qe = $qb->expr();

        return $qb
            ->andWhere($qe->neq('p.isRemove', ':status'))
            ->setParameters([
                'status' => true
            ])
            ->getQuery();
    }
}
