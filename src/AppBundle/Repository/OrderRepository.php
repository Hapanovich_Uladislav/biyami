<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class OrderRepository extends EntityRepository
{
    public function getOrderListQuery()
    {
        $qb = $this->createQueryBuilder('o');
        $qe = $qb->expr();

        return $qb
            ->addSelect('o')
            ->andWhere($qe->neq('o.isRemove', ':status'))
            ->setParameters([
                'status' => true
            ])
            ->addOrderBy('o.id', 'DESC')
            ->getQuery();
    }
}
