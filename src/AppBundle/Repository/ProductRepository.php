<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class ProductRepository extends EntityRepository
{
    public function getAllProductsQuery()
    {
        $qb = $this->createQueryBuilder('p');
        $qe = $qb->expr();

        return $qb
            ->addSelect('p, c')
            ->leftJoin('p.category', 'c')
            ->andWhere($qe->neq('p.isRemove', ':status'))
            ->setParameters([
                'status' => true
            ])
            ->getQuery();
    }
}
