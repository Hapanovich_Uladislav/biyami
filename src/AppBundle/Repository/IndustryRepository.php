<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class IndustryRepository extends EntityRepository
{
    public function findAllIndustries()
    {
        $qb = $this->createQueryBuilder('i');
        $qe = $qb->expr();

        return $qb
            ->select('i')
            ->andWhere($qe->neq('i.isRemove', ':status'))
            ->setParameters([
                'status' => true
            ])
            ->getQuery()
            ->getResult();
    }

    public function getAllIndustriesQuery()
    {
        $qb = $this->createQueryBuilder('i');
        $qe = $qb->expr();

        return $qb
            ->select('i')
            ->andWhere($qe->neq('i.isRemove', ':status'))
            ->setParameters([
                'status' => true
            ])
            ->getQuery();
    }
}
