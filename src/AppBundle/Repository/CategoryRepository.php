<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class CategoryRepository extends EntityRepository
{
    public function findAllCategories()
    {
        $qb = $this->createQueryBuilder('c');
        $qe = $qb->expr();

        return $qb
            ->select('c')
            ->andWhere($qe->neq('c.isRemove', ':status'))
            ->setParameters([
                'status' => true
            ])
            ->getQuery()
            ->getResult();
    }

    public function getAllCategoriesQuery()
    {
        $qb = $this->createQueryBuilder('c');
        $qe = $qb->expr();

        return $qb
            ->select('c')
            ->andWhere($qe->neq('c.isRemove', ':status'))
            ->setParameters([
                'status' => true
            ])
            ->addOrderBy('c.industry')
            ->getQuery();
    }
}
