<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class NewsRepository extends EntityRepository
{
    public function lastNews(int $count = 3)
    {
        $qb = $this->createQueryBuilder('n');
        $qe = $qb->expr();

        return $qb
            ->addSelect('n')
            ->andWhere($qe->neq('n.isRemove', ':status'))
            ->andWhere($qe->neq('n.isHide', ':status'))
            ->setParameters([
                'status' => true
            ])
            ->setMaxResults($count)
            ->addOrderBy('n.createdAt', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function getAllNewsQuery()
    {
        $qb = $this->createQueryBuilder('n');
        $qe = $qb->expr();

        return $qb
            ->addSelect('n')
            ->andWhere($qe->neq('n.isRemove', ':status'))
            ->setParameters([
                'status' => true
            ])
            ->getQuery();
    }
}
