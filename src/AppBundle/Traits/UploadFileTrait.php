<?php

namespace AppBundle\Traits;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Trait UploadFileTrait.
 */
trait UploadFileTrait
{
    /**
     * @var File
     *
     * @Assert\File(mimeTypes={"image/png", "image/jpeg"})
     */
    public $file;

    /**
     * @return void
     */
    public function resetFile(): void
    {
        $this->file = null;
    }

    /**
     * @return File|null
     */
    public function getFile(): ?File
    {
        return $this->file;
    }

    /**
     * @return void
     */
    public function upload(): void
    {
        /** @var UploadedFile $file */
        $file = $this->getFile();

        if (null === $file) {
            return;
        }

        $extension = substr($file->getClientOriginalName(), strripos($file->getClientOriginalName(), '.'));
        $name = md5_file($file) . $extension;

        $file->move($this->getUploadRootDir(), $name);

        $this->setFilePath($name);
        $this->resetFile();
    }
}