<?php

namespace AppBundle\Commands;

use AppBundle\Entity\User;
use Symfony\Component\Console\{
    Command\Command, Input\InputInterface, Output\OutputInterface
};

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SetPasswordCommand extends Command
{
    private $em;
    private $encoder;

    public function __construct(EntityManagerInterface $em, UserPasswordEncoderInterface $encoder)
    {
        $this->em = $em;
        $this->encoder = $encoder;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('new:password')
            ->addArgument('password');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $password = $input->getArgument('password');

        /** @var User $admin */
        $admin = $this->em->getRepository(User::class)->find(1);

        $encoded = $this->encoder->encodePassword($admin, $password);
        $admin->setPassword($encoded);

        $this->em->flush();

        $output->writeln(['New password: ' . $password]);
    }
}
