<?php

namespace AppBundle\Form;

use AppBundle\Entity\Industry;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use AppBundle\Entity\Category;

class CategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' => false,
                'required' => true,
                'attr' => [
                    'placeholder' => 'Введите название',
                ],
                'empty_data' => null,
                'invalid_message' => 'Неправильное навзание'
            ])
//            ->add('type', ChoiceType::class, [
//                'choices' => [
//                    'Мясоперерабатывающая отрасль' => '1',
//                    'Хлебопекарная отрасль' => '2',
//                    'Соевые продукты' => '3'
//                ],
//                'required' => true,
//                'label' => false,
//                'empty_data' => null,
//                'invalid_message' => 'Неправильно выбран тип',
//            ])
            ->add('industry', EntityType::class, [
                'label' => false,
                'required' => true,
                'class' => Industry::class,
                'choice_label' => 'title',
            ])
            ->add('file', FileType::class, [
                'required' => false,
                'label' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Category::class,
        ));
    }
}