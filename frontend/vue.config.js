module.exports = {
  outputDir: '../web/build',
  publicPath: process.env.NODE_ENV === 'production' ? '/build/' : '/',
  chainWebpack: (config) => {
    config.plugins.delete('prefetch');

    config.module
      .rule('media')
      .test(/\.(mp4|webm|ogg|mp3|wav|flac|aac|m4a)(\?.*)?$/);

    config.module
      .rule('vue')
      .use('vue-loader')
      .loader('vue-loader')
      .tap((options) => Object.assign(
        options,
        {
          transformToRequire: { audio: 'src' },
          transformAssetUrls: { audio: 'src' }
        }
      ));
  },
  devServer: {
    proxy: process.env.PROXY_SERVER_URL
  }
};
