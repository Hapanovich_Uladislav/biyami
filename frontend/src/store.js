import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    initData: null,
    partners: []
  },
  actions: {
    fetchInitData({ commit }) {
      axios.get('/api/init')
        .then(({ data }) => {
          commit('setInitData', data);
        })
    },
    fetchPartnerList({ commit }) {
      axios.get('/partner/list')
        .then(({ data }) => {
          commit('setPartnerList', data);
        })
    }
  },
  mutations: {
    setInitData(state, initData) {
      state.initData = initData;
    },
    setPartnerList(state, initData) {
      state.partners = initData;
    }
  }
})
