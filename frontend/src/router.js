import Vue from 'vue';
import Router from 'vue-router';
import Meta from 'vue-meta';
import CallRequestForm from '@/components/CallRequestForm.vue';

Vue.use(Router);
Vue.use(Meta);

export default new Router({
  mode: 'history',
  base: '/',
  scrollBehavior() {
    return {
      x: 0,
      y: 0
    };
  },
  routes: [
    {
      path: '/',
      name: 'home',
        components: {
        default: () => import(/* webpackChunkName: "home" */ './pages/Home.vue'),
        callRequestForm: CallRequestForm
      }
    },
    {
      path: '/about-company',
      name: 'aboutCompany',
      components: {
        default: () => import(/* webpackChunkName: "aboutCompany" */ './pages/AboutCompany.vue'),
        callRequestForm: CallRequestForm

      }
    },
    {
      path: '/news',
      name: 'news',
      components: {
        default: () => import(/* webpackChunkName: "news" */ './pages/News.vue'),
        callRequestForm: CallRequestForm
      }
    },
    {
      path: '/news/view/:id',
      name: 'newsView',
      components: {
        default: () => import(/* webpackChunkName: "news-view" */ './pages/NewsView.vue'),
        callRequestForm: CallRequestForm
      }
    },
    {
      path: '/partners',
      name: 'partners',
      components: {
        default: () => import(/* webpackChunkName: "partners" */ './pages/Partners.vue'),
        callRequestForm: CallRequestForm

      }
    },
    {
      path: '/products',
      name: 'products',
      components: {
        default: () => import(/* webpackChunkName: "products" */ './pages/Products.vue'),
        callRequestForm: CallRequestForm

      }
    },
    {
      path: '/contacts',
      name: 'contacts',
      component: () => import(/* webpackChunkName: "contacts" */ './pages/Contacts.vue')
    },
    {
      path: '/*',
      name: 'notFound',
      component: () => import(/* webpackChunkName: "notFound" */'./pages/NotFound.vue')
    }
  ]
});
